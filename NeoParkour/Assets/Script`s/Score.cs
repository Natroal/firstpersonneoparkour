﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    public static int theScore;
    public Text scoreText;

    void Update()
    {
        scoreText.text = "Score: " + theScore;
    }
}
