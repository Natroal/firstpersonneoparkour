﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RespawnTimer : MonoBehaviour
{
    [SerializeField] GameObject player;

    [SerializeField] Transform spawnPoint;

    [SerializeField] float spawnValue;

    int por;

    void Update()
    {
        if (player.transform.position.y < -spawnValue)
        {
            RespawnPoint();
        }
    }

    void RespawnPoint()
    {
        transform.position = spawnPoint.position;
        //por = 20 * Score.theScore / 100;


        if(Score.theScore > 0){
         Score.theScore -= 200;
        } 
        else if (Score.theScore < 0){
         Score.theScore = 0;
        }


        SceneManager.LoadScene("Scene3");
    }

}
