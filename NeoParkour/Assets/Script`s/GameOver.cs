﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOver : MonoBehaviour
{

    public float tempo;
    public Text tiempo;
    bool active;
    public GameObject panel;

    // Start is called before the first frame update
    void Start()
    {
        tempo = 60f;
        panel.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        #region Tiempo
        tempo -= Time.deltaTime;
        if (tempo <= 0)
        {
            active = !active;
            panel.SetActive(true);
            Time.timeScale =  0f;
            Cursor.lockState = CursorLockMode.Confined;
            Cursor.visible = true;
        }
        tiempo.text = "Time left: " + tempo.ToString("0");
        #endregion
    }
}
