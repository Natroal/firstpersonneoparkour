﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointsGround : MonoBehaviour
{
    public GameObject Temp;
    public GameObject Point;
    public GameObject Sound;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            Temp.GetComponent<GameOver>().tempo += 5;
            Instantiate(Sound);
            Score.theScore += 300;
            Destroy(Point);
        }
    }
}
