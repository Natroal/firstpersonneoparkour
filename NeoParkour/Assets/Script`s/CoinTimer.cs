﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinTimer : MonoBehaviour
{
    public GameObject Temp;
    public GameObject SoundCoin;

    void Start()
    {
        
    }

    void Update()
    {
        transform.Rotate(new Vector3(0f, 180f, 0f) * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            Temp.GetComponent<GameOver>().tempo += 5;
            Instantiate(SoundCoin);
            Score.theScore += 100;
            Destroy(gameObject);
        }
    }

   
}
