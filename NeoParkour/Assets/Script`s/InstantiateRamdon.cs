﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstantiateRamdon : MonoBehaviour
{
    public Transform positionOb;

    public GameObject[] objectsToInstantiate;

    // Start is called before the first frame update
    void Start()
    {
        int n = Random.Range(0, objectsToInstantiate.Length);

        Instantiate(objectsToInstantiate[n], positionOb.position, objectsToInstantiate[n].transform.rotation);

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
